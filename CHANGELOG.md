# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2020-01-09)


### Features

* add Crash CRUD API endpoints ([88c5f80](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/88c5f80f2feee76a048cc704d3a43f9e7c7152a6))
* add delete Emergencies endpoint ([9bc3aea](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/9bc3aea77925e1237429bb82913572edf849a2e2))
* add Ermergencies getById and getAll endpoint ([975838d](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/975838d163c72a9db6d49cfbac30b95f3130c69a))
* add Fire CRUD API endpoints ([7f54752](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/7f547525335761c7aef20dd2d912e750e0fd31bd))
* add Fire Stations CRUD API endpoints ([3501223](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/350122308b91d60e9f875c347dff3f37009c4366))
* add post and patch sensors with validation ([85913b3](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/85913b3b1f0dee501b5cf491e3236f4aa94c3146))
* link fireTrucks to emergencies ([07e1133](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/07e1133fec0e4dbad84d375e67a2cbb200fd570e))
* Sensors CRUD API endpoints ([7e6500e](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/7e6500ee6526efcfdbf759383f36dd13669ee221))


### Bug Fixes

* check emergency exist before asign it to truck ([b5fc6ab](https://gitlab.com/cpe-projet-transversale/emergency-web-server/commit/b5fc6ab67ac3a311a4b9efd432d1e3ddc67b0bbc))
