module.exports = {
    type: 'postgres',
    host: process.env.TYPEORM_HOST || 'localhost',
    port: process.env.TYPEORM_PORT || 5432,
    username: process.env.TYPEORM_USERNAME || 'admin',
    password: process.env.TYPEORM_PASSWORD || 'admin',
    database: process.env.TYPEORM_DATABASE || 'emergency_manager',
    entities: ['dist/**/*.entity{.ts,.js}'],
    keepConnectionAlive: true,
    synchronize: true,
  };
