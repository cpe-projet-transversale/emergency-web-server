import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Sensor extends BaseEntity {
  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @PrimaryGeneratedColumn()
  id: number;

  // Swagger
  @ApiProperty({ example: 45.764 })
  // TypeOrm
  @Column('float8')
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.8601 })
  // TypeOrm
  @Column('float8')
  long: string;

  // Swagger
  @ApiProperty({ example: 0 })
  // TypeOrm
  @Column({ default: 0 })
  value: number;
}
