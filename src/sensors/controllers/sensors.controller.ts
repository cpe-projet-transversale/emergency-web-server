import {
  Controller,
  Get,
  Param,
  Patch,
  Body,
  NotFoundException,
  UsePipes,
  Post,
} from '@nestjs/common';
import { SensorsService } from '../services/sensors.service';
import { Sensor } from '../models/sensors.entity';
import { EditSensorDto } from '../dtos/editSensor.dto';
import { DtoValidationPipe } from '../../shared/pipes/validations.pipe';
import { CreateSensorDto } from '../dtos/createSensor.dto';
import { plainToClass } from 'class-transformer';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';

@ApiTags('sensors')
@Controller('sensors')
export class SensorsController {
  constructor(private readonly sensorsService: SensorsService) {}

  @ApiOkResponse({ type: [Sensor] })
  @Get()
  async getAll(): Promise<Sensor[]> {
    return this.sensorsService.getAll();
  }

  @ApiOkResponse({ type: Sensor })
  @Post()
  @UsePipes(DtoValidationPipe)
  async add(@Body() partialSensor: CreateSensorDto): Promise<Sensor> {
    const sensor = plainToClass(Sensor, partialSensor);
    return await this.sensorsService.save(sensor);
  }

  @ApiOkResponse({ type: Sensor })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<Sensor> {
    const result = await this.sensorsService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: Sensor })
  @Patch(':id')
  @UsePipes(DtoValidationPipe)
  async editById(
    @Param('id') id: number,
    @Body() partialSensor: EditSensorDto,
  ): Promise<Sensor> {
    const sensor = await this.sensorsService.getById(id);

    if (!sensor) {
      throw new NotFoundException();
    }

    const newSensor = Object.assign(sensor, partialSensor);

    return await this.sensorsService.save(newSensor);
  }
}
