import { Min, Max, IsInt, IsLatitude, IsLongitude } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSensorDto {
  // Swagger
  @ApiProperty({ example: 45.764 })
  // Class-validator
  @IsLatitude()
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.8601 })
  // Class-validator
  @IsLongitude()
  long: string;

  // Swagger
  @ApiProperty()
  // Class-validator
  @IsInt()
  @Min(0)
  @Max(9)
  value: number;
}
