import { Injectable } from '@nestjs/common';
import { Sensor } from '../models/sensors.entity';

@Injectable()
export class SensorsService {
  constructor() {}

  async getAll(): Promise<Sensor[]> {
    const results = await Sensor.find();
    return results;
  }

  async getById(id: number): Promise<Sensor | false> {
    const result = await Sensor.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async save(sensor: Sensor) {
    return Sensor.save(sensor);
  }
}
