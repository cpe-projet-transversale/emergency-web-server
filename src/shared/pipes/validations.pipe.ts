import { ValidationPipe, ValidationPipeOptions } from '@nestjs/common';

const sharedConfig: ValidationPipeOptions = {
  whitelist: true,
};

export const DtoValidationPipe = new ValidationPipe({
  ...sharedConfig,
});
