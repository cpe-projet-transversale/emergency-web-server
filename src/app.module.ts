import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmergencyModule } from './sensors/sensors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IncidentsModule } from './incidents/incidents.module';
import { FireRespondersModule } from './fire-responders/fire-responders.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    EmergencyModule,
    IncidentsModule,
    FireRespondersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
