import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  TableInheritance,
  OneToMany,
  RelationId,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { FireTruck } from '../../fire-responders/models/fireTrucks.entity';

@Entity()
@TableInheritance({ column: { type: 'varchar2', name: 'type' } })
export abstract class Emergency extends BaseEntity {
  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @PrimaryGeneratedColumn()
  id: number;

  // Swagger
  @ApiProperty({ example: 'type' })
  // TypeOrm
  @Column()
  type: string;

  // Swagger
  @ApiProperty({ example: 45.764 })
  // TypeOrm
  @Column('float8')
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.8601 })
  // TypeOrm
  @Column('float8')
  long: string;

  // #####################################################
  // #    Emergency -> FireTruck[]
  // #####################################################

  // TypeOrm
  @OneToMany(
    type => FireTruck,
    fireTruck => fireTruck.emergency,
  )
  fireTrucks: FireTruck[];

  // Swagger
  @ApiProperty({ example: [1, 2, 4] })
  // TypeOrm
  @RelationId((emergency: Emergency) => emergency.fireTrucks)
  fireTrucksIds: number[];
}
