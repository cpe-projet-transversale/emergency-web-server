import { Emergency } from './emergencies.entity';
import { ChildEntity, Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@ChildEntity('fire')
export class Fire extends Emergency {
  // Swagger
  @ApiProperty({ example: 0 })
  // TypeOrm
  @Column({ default: 0 })
  value: number;
}
