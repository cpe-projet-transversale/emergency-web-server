import { Emergency } from './emergencies.entity';
import { ChildEntity, Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@ChildEntity('crash')
export class Crash extends Emergency {
  // Swagger
  @ApiProperty({ example: 0 })
  // TypeOrm
  @Column({ default: 0 })
  casualties: number;
}
