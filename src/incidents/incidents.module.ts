import { Module } from '@nestjs/common';
import { FiresController } from './controllers/fires.controller';
import { FiresService } from './services/fires.service';
import { CrashsController } from './controllers/crashs.controller';
import { CrashsService } from './services/crashs.service';
import { EmergenciesService } from './services/emergencies.service';
import { EmergenciesController } from './controllers/emergencies.controller';

@Module({
  controllers: [FiresController, CrashsController, EmergenciesController],
  providers: [FiresService, CrashsService, EmergenciesService],
  exports: [EmergenciesService],
})
export class IncidentsModule {}
