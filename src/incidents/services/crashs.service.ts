import { Injectable } from '@nestjs/common';
import { Emergency } from '../models/emergencies.entity';
import { Crash } from '../models/crashs.entity';

@Injectable()
export class CrashsService {
  constructor() {}

  async getAll(): Promise<Crash[]> {
    const results = await Crash.find();
    return results;
  }

  async getById(id: number): Promise<Crash | false> {
    const result = await Crash.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async save(crash: Crash) {
    return Crash.save(crash);
  }

  async remove(crash: Crash) {
    return Crash.remove(crash);
  }
}
