import { Injectable } from '@nestjs/common';
import { Emergency } from '../models/emergencies.entity';
import { Fire } from '../models/fires.entity';

@Injectable()
export class FiresService {
  constructor() {}

  async getAll(): Promise<Fire[]> {
    const results = await Fire.find();
    return results;
  }

  async getById(id: number): Promise<Fire | false> {
    const result = await Fire.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async save(fire: Fire) {
    return Fire.save(fire);
  }

  async remove(fire: Fire) {
    return Fire.remove(fire);
  }
}
