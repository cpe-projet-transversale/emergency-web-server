import { Injectable } from '@nestjs/common';
import { Emergency } from '../models/emergencies.entity';

@Injectable()
export class EmergenciesService {
  constructor() {}

  async getAll(): Promise<Emergency[]> {
    const results = await Emergency.find();
    return results;
  }

  async getById(id: number): Promise<Emergency | false> {
    const result = await Emergency.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async remove(emergency: Emergency) {
    return Emergency.remove(emergency);
  }
}
