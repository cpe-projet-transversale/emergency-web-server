import {
  Controller,
  Get,
  Param,
  Patch,
  Body,
  NotFoundException,
  UsePipes,
  Post,
  Delete,
} from '@nestjs/common';
import { DtoValidationPipe } from '../../shared/pipes/validations.pipe';
import { plainToClass } from 'class-transformer';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { CrashsService } from '../services/crashs.service';
import { Crash } from '../models/crashs.entity';
import { CreateCrashDto } from '../dtos/createCrash.dto';
import { EditCrashDto } from '../dtos/editCrash.dto';
import { Emergency } from '../models/emergencies.entity';

@ApiTags('emergencies/crashs')
@Controller('emergencies/crashs')
export class CrashsController {
  constructor(private readonly crashsService: CrashsService) {}

  @ApiOkResponse({ type: [Crash] })
  @Get()
  async getAll(): Promise<Crash[]> {
    return this.crashsService.getAll();
  }

  @ApiOkResponse({ type: Crash })
  @Post()
  @UsePipes(DtoValidationPipe)
  async add(@Body() partialCrash: CreateCrashDto): Promise<Crash> {
    const crash = plainToClass(Crash, partialCrash);
    return await this.crashsService.save(crash);
  }

  @ApiOkResponse({ type: Crash })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<Crash> {
    const result = await this.crashsService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: Crash })
  @Patch(':id')
  @UsePipes(DtoValidationPipe)
  async editById(
    @Param('id') id: number,
    @Body() partialCrash: EditCrashDto,
  ): Promise<Crash> {
    const crash = await this.crashsService.getById(id);

    if (!crash) {
      throw new NotFoundException();
    }

    const newCrash = Object.assign(crash, partialCrash);

    return await this.crashsService.save(newCrash);
  }

  @ApiOkResponse({ type: Crash })
  @Delete(':id')
  async deleteById(@Param('id') id: number): Promise<Crash> {

    const fire = await this.crashsService.getById(id);

    if (!fire) {
      throw new NotFoundException();
    }

    const result = await this.crashsService.remove(fire);

    return result;
  }
}
