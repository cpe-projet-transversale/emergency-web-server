import {
  Controller,
  Get,
  Param,
  Patch,
  Body,
  NotFoundException,
  UsePipes,
  Post,
  Delete,
} from '@nestjs/common';
import { DtoValidationPipe } from '../../shared/pipes/validations.pipe';
import { plainToClass } from 'class-transformer';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { FiresService } from '../services/fires.service';
import { Fire } from '../models/fires.entity';
import { CreateFireDto } from '../dtos/createFire.dto';
import { EditFireDto } from '../dtos/editFire.dto';

@ApiTags('emergencies/fires')
@Controller('emergencies/fires')
export class FiresController {
  constructor(private readonly firesService: FiresService) { }

  @ApiOkResponse({ type: [Fire] })
  @Get()
  async getAll(): Promise<Fire[]> {
    return this.firesService.getAll();
  }

  @ApiOkResponse({ type: Fire })
  @Post()
  @UsePipes(DtoValidationPipe)
  async add(@Body() partialFire: CreateFireDto): Promise<Fire> {
    const fire = plainToClass(Fire, partialFire);
    return await this.firesService.save(fire);
  }

  @ApiOkResponse({ type: Fire })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<Fire> {
    const result = await this.firesService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: Fire })
  @Patch(':id')
  @UsePipes(DtoValidationPipe)
  async editById(
    @Param('id') id: number,
    @Body() partialFire: EditFireDto,
  ): Promise<Fire> {
    const fire = await this.firesService.getById(id);

    if (!fire) {
      throw new NotFoundException();
    }

    const newFire = Object.assign(fire, partialFire);

    return await this.firesService.save(newFire);
  }

  @ApiOkResponse({ type: Fire })
  @Delete(':id')
  async deleteById(@Param('id') id: number): Promise<Fire> {

    const fire = await this.firesService.getById(id);

    if (!fire) {
      throw new NotFoundException();
    }

    const result = await this.firesService.remove(fire);

    return result;
  }
}
