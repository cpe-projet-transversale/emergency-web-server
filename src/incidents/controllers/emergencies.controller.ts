import { Controller, Get, Param, NotFoundException, Delete } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { EmergenciesService } from '../services/emergencies.service';
import { Emergency } from '../models/emergencies.entity';

@ApiTags('emergencies')
@Controller('emergencies')
export class EmergenciesController {
  constructor(private readonly emergenciesService: EmergenciesService) {}

  @ApiOkResponse({ type: [Emergency] })
  @Get()
  async getAll(): Promise<Emergency[]> {
    return this.emergenciesService.getAll();
  }

  @ApiOkResponse({ type: Emergency })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<Emergency> {
    const result = await this.emergenciesService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: Emergency })
  @Delete(':id')
  async deleteById(@Param('id') id: number): Promise<Emergency> {

    const emergency = await this.emergenciesService.getById(id);

    if (!emergency) {
      throw new NotFoundException();
    }

    const result = await this.emergenciesService.remove(emergency);

    return result;
  }
}
