import {
  IsInt,
  IsLatitude,
  IsLongitude,
  IsPositive,
  IsOptional,
} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class EditCrashDto {
  // Swagger
  @ApiPropertyOptional({ example: 45.764 })
  // Class-validator
  @IsOptional()
  @IsLatitude()
  lat?: string;

  // Swagger
  @ApiPropertyOptional({ example: 4.8601 })
  // Class-validator
  @IsOptional()
  @IsLongitude()
  long?: string;

  // Swagger
  @ApiPropertyOptional()
  // Class-validator
  @IsOptional()
  @IsInt()
  @IsPositive()
  casualties?: number;
}
