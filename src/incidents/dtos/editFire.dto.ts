import {
  Min,
  Max,
  IsInt,
  IsLatitude,
  IsLongitude,
  IsOptional,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class EditFireDto {
  // Swagger
  @ApiPropertyOptional({ example: 45.764 })
  // Class-validator
  @IsLatitude()
  @IsOptional()
  lat?: string;

  // Swagger
  @ApiPropertyOptional({ example: 4.8601 })
  // Class-validator
  @IsLongitude()
  @IsOptional()
  long?: string;

  // Swagger
  @ApiPropertyOptional()
  // Class-validator
  @IsInt()
  @Min(0)
  @Max(9)
  @IsOptional()
  value?: number;
}
