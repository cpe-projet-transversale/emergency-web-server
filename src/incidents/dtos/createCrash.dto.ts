import { IsInt, IsLatitude, IsLongitude, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCrashDto {
  // Swagger
  @ApiProperty({ example: 45.764 })
  // Class-validator
  @IsLatitude()
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.8601 })
  // Class-validator
  @IsLongitude()
  long: string;

  // Swagger
  @ApiProperty()
  // Class-validator
  @IsInt()
  @IsPositive()
  casualties: number;
}
