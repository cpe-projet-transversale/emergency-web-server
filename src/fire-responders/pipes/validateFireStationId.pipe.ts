import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { FireStationsService } from '../services/fireStations.service';

@Injectable()
export class ValidateFireStationIdPipe implements PipeTransform {
  constructor(private readonly fireStationsService: FireStationsService) {}

  async transform(value: any, metadata: ArgumentMetadata) {
    if (value.fireStationId) {
      const fireStation = await this.fireStationsService.getById(
        value.fireStationId,
      );

      if (!fireStation) {
        throw new BadRequestException();
      }
    }

    return value;
  }
}
