import {
  Controller,
  Get,
  Param,
  Patch,
  Body,
  NotFoundException,
  UsePipes,
  Post,
} from '@nestjs/common';
import { FireTrucksService } from '../services/fireTrucks.service';
import { FireTruck } from '../models/fireTrucks.entity';
import { EditFireTruckDto } from '../dtos/editFireTruck.dto';
import { DtoValidationPipe } from '../../shared/pipes/validations.pipe';
import { CreateFireTruckDto } from '../dtos/createFireTruck.dto';
import { plainToClass } from 'class-transformer';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { ValidateFireStationIdPipe } from '../pipes/validateFireStationId.pipe';
import { EmergenciesService } from '../../incidents/services/emergencies.service';

@ApiTags('fireTrucks')
@Controller('fireTrucks')
export class FireTrucksController {
  constructor(private readonly fireTrucksService: FireTrucksService,
              private readonly emergenciesService: EmergenciesService) {}

  @ApiOkResponse({ type: [FireTruck] })
  @Get()
  async getAll(): Promise<FireTruck[]> {
    return this.fireTrucksService.getAll();
  }

  @ApiOkResponse({ type: FireTruck })
  @Post()
  @UsePipes(DtoValidationPipe, ValidateFireStationIdPipe)
  async add(@Body() partialFireTruck: CreateFireTruckDto): Promise<FireTruck> {
    const fireTruck = plainToClass(FireTruck, partialFireTruck);

    return await this.fireTrucksService.save(fireTruck);
  }

  @ApiOkResponse({ type: FireTruck })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<FireTruck> {
    const result = await this.fireTrucksService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: FireTruck })
  @Patch(':id')
  @UsePipes(DtoValidationPipe, ValidateFireStationIdPipe)
  async editById(
    @Param('id') id: number,
    @Body() partialFireTruck: EditFireTruckDto,
  ): Promise<FireTruck> {
    const fireTruck = await this.fireTrucksService.getById(id);

    if (!fireTruck) {
      throw new NotFoundException();
    }

    if (partialFireTruck.emergencyId) {
      const emergency = await this.emergenciesService.getById(partialFireTruck.emergencyId);
      if (! emergency) { throw new NotFoundException(`No Emergency with the id: ${partialFireTruck.emergencyId}`) }
    }

    const newFireTruck = Object.assign(fireTruck, partialFireTruck);

    return await this.fireTrucksService.save(newFireTruck);
  }
}
