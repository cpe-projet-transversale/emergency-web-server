import {
  Controller,
  Get,
  Param,
  Patch,
  Body,
  NotFoundException,
  UsePipes,
  Post,
} from '@nestjs/common';
import { FireStationsService } from '../services/fireStations.service';
import { FireStation } from '../models/fireStations.entity';
import { EditFireStationDto } from '../dtos/editFireStation.dto';
import { DtoValidationPipe } from '../../shared/pipes/validations.pipe';
import { CreateFireStationDto } from '../dtos/createFireStation.dto';
import { plainToClass } from 'class-transformer';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';

@ApiTags('fireStations')
@Controller('fireStations')
export class FireStationsController {
  constructor(private readonly fireStationsService: FireStationsService) {}

  @ApiOkResponse({ type: [FireStation] })
  @Get()
  async getAll(): Promise<FireStation[]> {
    return this.fireStationsService.getAll();
  }

  @ApiOkResponse({ type: FireStation })
  @Post()
  @UsePipes(DtoValidationPipe)
  async add(
    @Body() partialFireStation: CreateFireStationDto,
  ): Promise<FireStation> {
    const fireStation = plainToClass(FireStation, partialFireStation);
    return await this.fireStationsService.save(fireStation);
  }

  @ApiOkResponse({ type: FireStation })
  @Get(':id')
  async getById(@Param('id') id: number): Promise<FireStation> {
    const result = await this.fireStationsService.getById(id);

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @ApiOkResponse({ type: FireStation })
  @Patch(':id')
  @UsePipes(DtoValidationPipe)
  async editById(
    @Param('id') id: number,
    @Body() partialFireStation: EditFireStationDto,
  ): Promise<FireStation> {
    const fireStation = await this.fireStationsService.getById(id);

    if (!fireStation) {
      throw new NotFoundException();
    }

    const newFireStation = Object.assign(fireStation, partialFireStation);

    return await this.fireStationsService.save(newFireStation);
  }
}
