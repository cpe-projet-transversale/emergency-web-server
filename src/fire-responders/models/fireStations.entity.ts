import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  OneToMany,
  RelationId,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { FireTruck } from './fireTrucks.entity';

@Entity()
export class FireStation extends BaseEntity {
  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @PrimaryGeneratedColumn()
  id: number;

  // Swagger
  @ApiProperty({ example: 45.766042 })
  // TypeOrm
  @Column('float8')
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.905505 })
  // TypeOrm
  @Column('float8')
  long: string;

  // Swagger
  @ApiProperty({ example: 'Caserne de Cusset' })
  // TypeOrm
  @Column()
  name: string;

  // #####################################################
  // #    FireStation -> FireTruck[]
  // #####################################################

  // TypeOrm
  @OneToMany(
    type => FireTruck,
    fireTruck => fireTruck.fireStation,
  )
  fireTrucks: FireTruck[];

  // Swagger
  @ApiProperty({ example: [1, 2, 4] })
  // TypeOrm
  @RelationId((fireStation: FireStation) => fireStation.fireTrucks)
  fireTrucksIds: number[];
}
