import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  ManyToOne,
  JoinColumn,
  RelationId,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { FireStation } from './fireStations.entity';
import { Emergency } from '../../incidents/models/emergencies.entity';

@Entity()
export class FireTruck extends BaseEntity {
  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @PrimaryGeneratedColumn()
  id: number;

  // Swagger
  @ApiProperty({ example: 45.766042 })
  // TypeOrm
  @Column('float8')
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.905505 })
  // TypeOrm
  @Column('float8')
  long: string;

  // #####################################################
  // #    FireTruck[] -> FireStation
  // #####################################################

  // Swagger
  // TypeOrm
  @ManyToOne(
    type => FireStation,
    fireStation => fireStation.fireTrucks,
    { nullable: false },
  )
  @JoinColumn()
  fireStation: string;

  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @Column({ nullable: true })
  @RelationId((fireTruck: FireTruck) => fireTruck.fireStation)
  fireStationId: number;

  // #####################################################
  // #    FireTruck[] -> Emergency
  // #####################################################

  // Swagger
  // TypeOrm
  @ManyToOne(
    type => Emergency,
    emergency => emergency.fireTrucks,
    { onDelete: 'SET NULL' }
  )
  @JoinColumn()
  emergency: string;

  // Swagger
  @ApiProperty({ example: 1 })
  // TypeOrm
  @Column({ nullable: true })
  @RelationId((fireTruck: FireTruck) => fireTruck.emergency)
  emergencyId: number;
}
