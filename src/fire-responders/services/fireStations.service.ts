import { Injectable } from '@nestjs/common';
import { FireStation } from '../models/fireStations.entity';
@Injectable()
export class FireStationsService {
  constructor() {}

  async getAll(): Promise<FireStation[]> {
    const results = await FireStation.find();
    return results;
  }

  async getById(id: number): Promise<FireStation | false> {
    const result = await FireStation.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async save(fireStation: FireStation) {
    return FireStation.save(fireStation);
  }
}
