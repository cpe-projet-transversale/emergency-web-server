import { Injectable } from '@nestjs/common';
import { FireTruck } from '../models/fireTrucks.entity';
@Injectable()
export class FireTrucksService {
  constructor() {}

  async getAll(): Promise<FireTruck[]> {
    const results = await FireTruck.find();
    return results;
  }

  async getById(id: number): Promise<FireTruck | false> {
    const result = await FireTruck.findOne(id);
    if (!result) {
      return false;
    }
    return result;
  }

  async save(fireTruck: FireTruck) {
    return FireTruck.save(fireTruck);
  }
}
