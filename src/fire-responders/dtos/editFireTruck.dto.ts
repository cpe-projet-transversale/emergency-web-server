import {
  IsLatitude,
  IsLongitude,
  IsPositive,
  IsOptional,
} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class EditFireTruckDto {
  // Swagger
  @ApiPropertyOptional({ example: 45.766042 })
  // Class-validator
  @IsOptional()
  @IsLatitude()
  lat?: string;

  // Swagger
  @ApiPropertyOptional({ example: 4.905505 })
  // Class-validator
  @IsOptional()
  @IsLongitude()
  long?: string;

  // Swagger
  @ApiPropertyOptional({ example: 1 })
  // Class-validator
  @IsOptional()
  @IsPositive()
  fireStationId?: number;

  // Swagger
  @ApiPropertyOptional({ example: 1 })
  // Class-validator
  @IsOptional()
  @IsPositive()
  emergencyId?: number | null;
}
