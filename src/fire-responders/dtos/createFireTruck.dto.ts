import { IsLatitude, IsLongitude, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateFireTruckDto {
  // Swagger
  @ApiProperty({ example: 45.766042 })
  // Class-validator
  @IsLatitude()
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.905505 })
  // Class-validator
  @IsLongitude()
  long: string;

  // Swagger
  @ApiProperty({ example: 1 })
  // Class-validator
  @IsPositive()
  fireStationId: number;
}
