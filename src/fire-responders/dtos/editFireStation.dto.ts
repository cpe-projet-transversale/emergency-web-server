import { IsLatitude, IsLongitude, Length, IsOptional } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class EditFireStationDto {
  // Swagger
  @ApiPropertyOptional({ example: 45.766042 })
  // Class-validator
  @IsOptional()
  @IsLatitude()
  lat?: string;

  // Swagger
  @ApiPropertyOptional({ example: 4.905505 })
  // Class-validator
  @IsOptional()
  @IsLongitude()
  long?: string;

  // Swagger
  @ApiPropertyOptional({ example: 'Caserne de Cusset' })
  // Class-validator
  @IsOptional()
  @Length(1, 30)
  name?: string;
}
