import { IsLatitude, IsLongitude, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateFireStationDto {
  // Swagger
  @ApiProperty({ example: 45.766042 })
  // Class-validator
  @IsLatitude()
  lat: string;

  // Swagger
  @ApiProperty({ example: 4.905505 })
  // Class-validator
  @IsLongitude()
  long: string;

  // Swagger
  @ApiProperty({ example: 'Caserne de Cusset' })
  // Class-validator
  @Length(1, 30)
  name: string;
}
