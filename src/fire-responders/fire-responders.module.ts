import { Module } from '@nestjs/common';
import { FireStationsService } from './services/fireStations.service';
import { FireStationsController } from './controllers/fireStations.controller';
import { FireTrucksController } from './controllers/fireTrucks.controller';
import { FireTrucksService } from './services/fireTrucks.service';
import { IncidentsModule } from '../incidents/incidents.module';

@Module({
  controllers: [FireStationsController, FireTrucksController],
  providers: [FireStationsService, FireTrucksService],
  imports: [IncidentsModule]
})
export class FireRespondersModule {}
