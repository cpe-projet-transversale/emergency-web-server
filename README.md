<p align="center">
  <img src="https://nestjs.com/img/logo.svg" width="320" alt="Nest Logo" />
</p>

# Installation

## Database

Start Postgres Database  and PostgresMyAdmin

``` bash
docker-compose up
```

Recreate/Reset DB with script folder

``` bash
docker-compose down
docker-compose up --force-recreate --renew-anon-volumes
```

Go to pgAdmin at http://localhost:5050
1) Login admin/admin
2) Add new server
3) name: DB
4) Connexion -> host: postgres
5) Connexion -> user: postgres
6) Connexion -> password: password


## Web Server

```bash
$ npm install
```

# Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

# Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```